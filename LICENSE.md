All artwork here is under the [Creative Commons Attribution-ShareAlike
4.0](https://creativecommons.org/licenses/by-sa/4.0/). This artwork may also be
based on artwork on other licenses. In such case, license of other artwork
shall permit it to be modified and distributed with other terms.

Authors of particular items, and notes, if any:

<table>
  <tr>
    <td><a href="brovser.png">brovser.png</a></td>
    <td>Jakub Kaszycki</td>
  </tr>
  <tr>
    <td><a href="brovser.xcf">brovser.xcf</a></td>
    <td>Jakub Kaszycki</td>
  </tr>
  <tr>
    <td><a href="images.png">images.png</a></td>
    <td>Jakub Kaszycki (note: <a href="https://pixabay.com/en/blue-frame-ornate-antique-design-1007695/">Original image</a>)</td>
  </tr>
  <tr>
    <td><a href="images.xcf">images.xcf</a></td>
    <td>Jakub Kaszycki (note: <a href="https://pixabay.com/en/blue-frame-ornate-antique-design-1007695/">Original image</a>)</td>
  </tr>
  <tr>
    <td><a href="libbhttp.png">libbhttp.png</a></td>
    <td>Jakub Kaszycki (note: <a href="https://pixabay.com/en/cpu-processor-intel-amd-chip-152656/">Original image</a>)</td>
  </tr>
  <tr>
    <td><a href="libbhttp.xcf">libbhttp.xcf</a></td>
    <td>Jakub Kaszycki (note: <a href="https://pixabay.com/en/cpu-processor-intel-amd-chip-152656/">Original image</a>)</td>
  </tr>
</table>

Note that fonts used in the texts on images may be subject to different
licenses. Even though, if a font appears on an image here, it means it was
checked that it allows to use it.
